import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';
import run from '@rollup/plugin-run';
import dts from 'rollup-plugin-dts';
import del from 'rollup-plugin-delete';
import path from 'path';
import pkg from './package.json';

// --- config ---
const entry = 'src/index.ts';
// --- config ---

const dev = process.env.ROLLUP_WATCH === 'true';
const external = Object.keys(pkg.dependencies ?? {});

/** @type {import('rollup').RollupOptions[]} */
const config = [
	{
		input: entry,
		output: [
			{
				file: pkg.main,
				format: 'es'
			}
		],
		external,
		plugins: [
			resolve(),
			commonjs(),
			typescript({
				tsconfig: './tsconfig.json',
				declaration: !dev,
				declarationDir: dev ? undefined : '.types'
			}),
			dev &&
				run({
					execArgv: ['-r', 'source-map-support/register', '-r', 'dotenv/config']
				})
		]
	}
];

// genereate types on build
if (!dev) {
	const outDir = path.dirname(pkg.main ?? pkg.module);
	config.push({
		input: `${outDir}/.types/${path.parse(entry).name}.d.ts`,
		output: {
			file: pkg.types,
			format: 'es'
		},
		external,
		plugins: [dts(), del({ targets: `${outDir}/.types`, hook: 'buildEnd' })]
	});
}

export default config;
