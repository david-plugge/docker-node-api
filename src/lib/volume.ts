import { Modem } from './api/modem';

import * as Inspect from './types/volume/inspectVolume';
import * as Remove from './types/volume/removeVolume';

export class Volume {
	private readonly modem: Modem;
	readonly name: string;

	constructor(modem: Modem, name: string) {
		this.modem = modem;
		this.name = name;
	}

	public async inspect(
		options?: Inspect.QueryParams
	): Promise<{ success: boolean; data?: Inspect.Response }> {
		const { code, data } = await this.modem.request<Inspect.Response>({
			method: 'GET',
			path: `/volumes/${this.name}`,
			query: options
		});
		return { success: code === 200, data };
	}

	public async remove(options?: Remove.QueryParams) {
		const { code } = await this.modem.request({
			method: 'DELETE',
			path: `/volumes/${this.name}`,
			query: options
		});
		return { success: code === 204 };
	}
}
