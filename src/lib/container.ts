import type { Stream } from 'node:stream';
import { EventEmitter } from 'node:events';

import { Modem } from './api/modem';

import * as Inspect from './types/container/inspectContainer';
import * as Start from './types/container/startContainer';
import * as Stop from './types/container/stopContainer';
import * as Restart from './types/container/restartContainer';
import * as Kill from './types/container/killContainer';
import * as Attach from './types/container/attachToContainer';
import * as Remove from './types/container/removeContainer';

export interface AttachEventEmitter {
	on(event: 'stdout', listener: (data: string) => void): this;
	on(event: 'stderr', listener: (data: string) => void): this;
	emit(event: 'stdin', data: string): boolean;
}

export class Container {
	private readonly modem: Modem;
	readonly id: string;

	constructor(modem: Modem, id: string) {
		this.modem = modem;
		this.id = id;
	}

	public async inspect(
		options?: Inspect.QueryParams
	): Promise<{ success: boolean; data?: Inspect.Response }> {
		const { code, data } = await this.modem.request<Inspect.Response>({
			method: 'GET',
			path: `/containers/${this.id}/json`,
			query: options
		});
		return { success: code === 200, data };
	}

	public async start(options?: Start.QueryParams): Promise<{ success: boolean }> {
		const { code } = await this.modem.request({
			method: 'POST',
			path: `/containers/${this.id}/start`,
			query: options
		});
		return { success: code === 204 || code === 304 };
	}
	public async stop(options?: Stop.QueryParams): Promise<{ success: boolean }> {
		const { code } = await this.modem.request({
			method: 'POST',
			path: `/containers/${this.id}/stop`,
			query: options
		});
		return { success: code === 204 || code === 304 };
	}
	public async restart(options?: Restart.QueryParams): Promise<{ success: boolean }> {
		const { code } = await this.modem.request({
			method: 'POST',
			path: `/containers/${this.id}/restart`,
			query: options
		});
		return { success: code === 204 };
	}
	public async kill(options?: Kill.QueryParams): Promise<{ success: boolean }> {
		const { code } = await this.modem.request({
			method: 'POST',
			path: `/containers/${this.id}/kill`,
			query: options
		});
		return { success: code === 204 };
	}
	public async attach(options?: Attach.QueryParams): Promise<AttachEventEmitter> {
		const stream = await this.modem.stream({
			method: 'POST',
			path: `/containers/${this.id}/attach`,
			query: options
		});
		const emitter = new EventEmitter();
		emitter.on('stdin', (data) => {
			stream.write(data);
		});

		demuxStream(stream, ({ std, data }) => {
			emitter.emit(std, data);
		});

		return emitter;
	}
	public async remove(options?: Remove.QueryParams): Promise<{ success: boolean }> {
		const { code, data } = await this.modem.request({
			method: 'DELETE',
			path: `/containers/${this.id}`,
			query: options
		});
		console.log(data);

		return code === 204 ? { success: true } : { success: false };
	}
}

export const demuxStream = (
	stream: Stream,
	stdout: NodeJS.WritableStream | ((data: { std: 'stdout' | 'stderr'; data: string }) => void),
	stderr?: NodeJS.WritableStream
) => {
	let nextDataType: number | null;
	let nextDataLength: number | null;
	let buffer = Buffer.from('');

	const useCb = typeof stdout === 'function';

	const processData = (data?: Uint8Array) => {
		if (data) buffer = Buffer.concat([buffer, data]);
		if (!nextDataType) {
			if (buffer.length >= 8) {
				const header = bufferSlice(8);
				nextDataType = header.readUInt8(0);
				nextDataLength = header.readUInt32BE(4);
				processData();
			}
		} else {
			if (nextDataLength && buffer.length >= nextDataLength) {
				const content = bufferSlice(nextDataLength);
				if (nextDataType === 1) {
					if (useCb) stdout({ std: 'stdout', data: content.toString() });
					else stdout.write(content);
				} else {
					if (useCb) stdout({ std: 'stderr', data: content.toString() });
					else if (stderr) stderr.write(content);
				}
				nextDataType = null;
				// It's possible we got a "data" that contains multiple messages
				// Process the next one
				processData();
			}
		}
	};

	const bufferSlice = (end: number) => {
		const out = buffer.slice(0, end);
		buffer = Buffer.from(buffer.slice(end, buffer.length));
		return out;
	};

	stream.on('data', processData);
};
