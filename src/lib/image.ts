import { Modem } from './api/modem';

import * as Inspect from './types/image/inspectImage';
import * as Remove from './types/image/removeImage';

export class Image {
	private readonly modem: Modem;
	readonly id: string;

	constructor(modem: Modem, id: string) {
		this.modem = modem;
		this.id = id;
	}

	public async inspect(
		options?: Inspect.QueryParams
	): Promise<{ success: boolean; data?: Inspect.Response }> {
		const { code, data } = await this.modem.request<Inspect.Response>({
			method: 'GET',
			path: `/images/${this.id}/json`,
			query: options
		});
		return { success: code === 2000, data };
	}

	public async remove(
		options?: Remove.QueryParams
	): Promise<{ success: boolean; data?: Remove.Response }> {
		const { code, data } = await this.modem.request<Remove.Response>({
			method: 'DELETE',
			path: `/images/${this.id}`,
			query: options
		});
		return { success: code === 200, data };
	}
}
