import { Container } from './container';
import { Modem, ModemOptions } from './api/modem';
import { Image } from './image';
import { Volume } from './volume';
import { Network } from './network';

import * as ListContainers from './types/container/listContainers';
import * as RemoveStoppedContainers from './types/container/removeStoppedContainers';
import * as CreateContainer from './types/container/createContainer';

import * as ListImages from './types/image/listImages';
import * as PullImage from './types/image/pullImage';
import * as CreateImage from './types/image/createImage';
import * as RemoveUnusedImages from './types/image/removeUnusedImages';

import * as CreateVolume from './types/volume/createVolume';
import * as ListVolumes from './types/volume/listVolumes';
import * as RemoveUnusedVolumes from './types/volume/removeUnusedVolumes';

import * as CreateNetwork from './types/network/createNetwork';
import * as ListNetworks from './types/network/listNetworks';
import * as RemoveUnusedNetworks from './types/network/removeUnusedNetworks';

import * as ListenForEvents from './types/system/event';

export { ModemOptions };

export class Docker {
	private readonly modem: Modem;

	constructor(options?: ModemOptions) {
		this.modem = new Modem(options);
	}

	// ----- CONTAINER -----
	public getContainer(id: string) {
		return new Container(this.modem, id);
	}
	public async listContainers(
		options: ListContainers.QueryParams = {}
	): Promise<{ code: number; success: boolean; data?: ListContainers.Response }> {
		const { code, data } = await this.modem.request<ListContainers.Response>({
			method: 'GET',
			path: '/containers/json',
			query: {
				all: true,
				...options
			}
		});
		return { code, data, success: code >= 200 && code < 300 };
	}

	public async createContainer(
		options: CreateContainer.RequestBody
	): Promise<
		| { code: number; success: true; data: Container }
		| { code: number; success: false; data: undefined }
	>;
	public async createContainer(
		name: CreateContainer.QueryParams['name'],
		options: CreateContainer.RequestBody
	): Promise<
		| { code: number; success: true; data: Container }
		| { code: number; success: false; data: undefined }
	>;
	public async createContainer(
		name?: CreateContainer.QueryParams['name'] | CreateContainer.RequestBody,
		options?: CreateContainer.RequestBody
	) {
		if (typeof name !== 'string') {
			options = name;
			name = undefined;
		} else {
			if (!/\/?[a-zA-Z0-9][a-zA-Z0-9_.-]+/.test(name)) {
				throw new Error('Name not allowed');
			}
		}
		const { code, data } = await this.modem.request({
			method: 'POST',
			path: '/containers/create',
			data: JSON.stringify(options),
			headers: { 'Content-Type': 'application/json' },

			query: name ? { name } : {}
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	public async removeStoppedContainers(
		options: RemoveStoppedContainers.QueryParams = {}
	): Promise<{ code: number; success: boolean; data?: RemoveStoppedContainers.Response }> {
		const { code, data } = await this.modem.request<RemoveStoppedContainers.Response>({
			method: 'POST',
			path: '/containers/prune',
			query: options
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	// ----- CONTAINER -----

	// ----- IMAGE -----
	public getImage(id: string) {
		return new Image(this.modem, id);
	}
	public async listImages(
		options: ListImages.QueryParams = {}
	): Promise<{ code: number; success: boolean; data?: ListImages.Response }> {
		const { code, data } = await this.modem.request<ListImages.Response>({
			method: 'GET',
			path: '/images/json',
			query: options
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	public async pullImage(
		options: PullImage.QueryParams = {},
		onProgress?: (data: unknown) => void
	) {
		return new Promise<void>((resolve, reject) => {
			this.modem
				.stream({
					method: 'POST',
					path: '/images/create',
					query: options
				})
				.then((stream) => {
					stream.on('error', (err) => reject(err));

					if (typeof onProgress === 'function') {
						stream.on('data', (data) => {
							const progress = data.toString();
							progress.split('\r\n').forEach((p: string) => {
								try {
									onProgress(JSON.parse(p));
								} catch (err) {
									/* eslint-disable no-empty */
								}
							});
						});
					}

					stream.on('end', () => {
						resolve();
					});
				});
		});
	}
	public async createImage(
		options: CreateImage.QueryParams = {}
	): Promise<{ code: number; success: boolean; data?: CreateImage.Response }> {
		const { code, data } = await this.modem.request<CreateImage.Response>({
			method: 'POST',
			path: '/images/create',
			query: options
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	public async removeUnusedImages(
		options: RemoveUnusedImages.QueryParams = {}
	): Promise<{ code: number; success: boolean; data?: RemoveUnusedImages.Response }> {
		const { code, data } = await this.modem.request<RemoveUnusedImages.Response>({
			method: 'POST',
			path: '/images/prune',
			query: options
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	// ----- IMAGE -----

	// ----- VOLUME -----
	public getVolume(id: string) {
		return new Volume(this.modem, id);
	}
	public async createVolume(
		options: CreateVolume.RequestBody
	): Promise<{ code: number; success: boolean; data?: CreateVolume.Response }> {
		const { code, data } = await this.modem.request<CreateVolume.Response>({
			method: 'POST',
			path: '/volumes/create',
			data: JSON.stringify(options)
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	public async listVolumes(): Promise<{
		code: number;
		success: boolean;
		data?: ListVolumes.Response;
	}> {
		const { code, data } = await this.modem.request<ListVolumes.Response>({
			method: 'GET',
			path: '/volumes'
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	public async removeUnusedVolumes(
		options: RemoveUnusedVolumes.QueryParams = {}
	): Promise<{ code: number; success: boolean; data?: RemoveUnusedVolumes.Response }> {
		const { code, data } = await this.modem.request<RemoveUnusedVolumes.Response>({
			method: 'POST',
			path: '/volumes/prune',
			query: options
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	// ----- VOLUME -----

	// ----- NETWORK -----
	public getNetwork(id: string) {
		return new Network(this.modem, id);
	}
	public async listNetworks(
		options: ListNetworks.QueryParams = {}
	): Promise<{ code: number; success: boolean; data?: ListNetworks.Response }> {
		const { code, data } = await this.modem.request<ListNetworks.Response>({
			method: 'GET',
			path: '/networks',
			query: options
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	public async createNetwork(
		options: CreateNetwork.RequestBody
	): Promise<{ code: number; success: boolean; data?: CreateNetwork.Response }> {
		const { code, data } = await this.modem.request<CreateNetwork.Response>({
			method: 'POST',
			path: '/networks/create',
			data: JSON.stringify(options)
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	public async removeUnusedNetworks(
		options: RemoveUnusedNetworks.QueryParams
	): Promise<{ code: number; success: boolean; data?: RemoveUnusedNetworks.Response }> {
		const { code, data } = await this.modem.request<RemoveUnusedNetworks.Response>({
			method: 'POST',
			path: '/networks/prune',
			query: options
		});
		return { code, data, success: code >= 200 && code < 300 };
	}
	// ----- NETWORK -----

	// ----- SYSTEM -----
	public async events(
		options: ListenForEvents.QueryParams,
		cb: (event: ListenForEvents.Response) => void
	) {
		const stream = await this.modem.stream({
			method: 'GET',
			path: '/events',
			query: options
		});
		// console.log(stream);
		stream.on('data', (chunk) => {
			const packet = JSON.parse(chunk.toString());
			cb(packet);
		});
	}
}
