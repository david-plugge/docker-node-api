import { Modem } from './api/modem';

import * as ConnectContainer from './types/network/connectContainer';
import * as DisonnectContainer from './types/network/disconnectContainer';
import * as Inspect from './types/network/inspectNetwork';
import * as Remove from './types/network/removeNetwork';

export class Network {
	private readonly modem: Modem;
	readonly id: string;

	constructor(modem: Modem, id: string) {
		this.modem = modem;
		this.id = id;
	}

	public async inspect(
		options?: Inspect.QueryParams
	): Promise<{ success: boolean; data?: Inspect.Response }> {
		const { code, data } = await this.modem.request<Inspect.Response>({
			method: 'GET',
			path: `/networks/${this.id}`,
			query: options
		});
		return { success: code === 200, data };
	}

	public async remove(options?: Remove.QueryParams) {
		const { code } = await this.modem.request({
			method: 'DELETE',
			path: `/networks/${this.id}`,
			query: options
		});
		return { success: code === 204 };
	}

	public async connect(
		config: ConnectContainer.RequestBody,
		options?: ConnectContainer.QueryParams
	) {
		const { code } = await this.modem.request({
			method: 'DELETE',
			path: `/networks/${this.id}/connect`,
			data: JSON.stringify(config),
			query: options
		});
		return { success: code === 200 };
	}

	public async disconnect(
		config: ConnectContainer.RequestBody,
		options?: DisonnectContainer.QueryParams
	) {
		const { code } = await this.modem.request({
			method: 'POST',
			path: `/networks/${this.id}/disconnect`,
			data: JSON.stringify(config),
			query: options
		});
		return { success: code === 200 };
	}
}
