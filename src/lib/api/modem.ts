import { parseSearchQuery, Query } from '../utils/url';
import { request } from 'node:http';
import { HttpDuplexStream } from './httpDuplex';

export interface ModemOptions {
	version?: string;
	socketPath?: string;
}

interface BuildRequestInput {
	path: string;
	headers?: Record<string, string>;
	method: string;
	query?: Query;
}

export interface ModemRequestInput {
	path: string;
	headers?: Record<string, string>;
	method: string;
	data?: string | Buffer;
	query?: Query;
}

export class Modem {
	private readonly socketPath: string;
	private readonly version: string;

	constructor(options: ModemOptions = {}) {
		this.socketPath = options.socketPath ?? '/var/run/docker.sock';
		this.version = options.version ?? 'v1.41';
	}

	private buildRequest(options: BuildRequestInput) {
		return request({
			path: `/${this.version}${options.path}`,
			method: options.method,
			search: options.query ? parseSearchQuery(options.query) : '',
			socketPath: this.socketPath,
			headers: options.headers
		});
	}

	public request<T = unknown>(options: ModemRequestInput) {
		return new Promise<{ code: number; data?: T }>((resolve, reject) => {
			const req = this.buildRequest(options);

			req.on('error', reject);
			req.on('response', (res) => {
				const isJson = !!res.headers['content-type']?.includes('application/json');
				const code = res.statusCode ?? 400;
				const chunks: Uint8Array[] = [];

				res.on('data', (chunk) => chunks.push(chunk));
				res.on('end', () => {
					if (chunks.length > 0) {
						const buffer = Buffer.concat(chunks);
						const result = buffer.toString();

						if (isJson) {
							try {
								resolve({ code, data: JSON.parse(result) });
							} catch (error) {
								reject(error);
							}
						} else {
							resolve({ code, data: result as unknown as T });
						}
					} else {
						resolve({ code });
					}
				});
			});
			const data = options.data;
			if (typeof data === 'string' || Buffer.isBuffer(data)) {
				req.write(data);
			}
			req.end();
		});
	}

	public stream(options: ModemRequestInput) {
		return new Promise<HttpDuplexStream & NodeJS.WritableStream>((resolve, reject) => {
			const req = this.buildRequest(options);
			req.on('error', (error) => {
				reject(error);
			});
			req.on('response', (res) => {
				const duplex = new HttpDuplexStream(req, res);
				resolve(duplex);
			});

			req.end();
		});
	}
}
