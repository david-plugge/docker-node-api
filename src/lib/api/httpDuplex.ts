import { ClientRequest, IncomingMessage } from 'node:http';
import { Duplex } from 'node:stream';

export class HttpDuplexStream extends Duplex {
	private readonly request: ClientRequest;
	private readonly response: IncomingMessage;

	constructor(request: ClientRequest, response: IncomingMessage) {
		super();
		this.request = request;
		this.response = response;

		this.emit('response', response);
		response.on('data', (chunk) => {
			if (!this.push(chunk)) {
				this.response.pause();
			}
		});
		response.on('end', () => this.push(null));
	}

	public _read(): void {
		if (this.response) {
			this.response.resume();
		}
	}

	public _write(
		chunk: unknown,
		encoding: BufferEncoding,
		callback: (error?: Error | null | undefined) => void
	): void {
		this.request.write(chunk, encoding, callback);
	}

	end(cb?: (() => void) | undefined): this;
	end(chunk: unknown, cb?: (() => void) | undefined): this;
	end(chunk: unknown, encoding?: BufferEncoding | undefined, cb?: (() => void) | undefined): this;
	end(chunk?: unknown, encoding?: unknown, cb?: unknown): this {
		const callback =
			typeof chunk === 'function' ? chunk : typeof encoding === 'function' ? encoding : cb;
		this.request.end(callback);
		return this;
	}

	public destroy(error?: Error | undefined): this {
		this.request.destroy(error);
		this.response.socket.destroy(error);
		return this;
	}
}
