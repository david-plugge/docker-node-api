export type PathParams = Record<string, never>;

export type QueryParams = {
	filters?: string;
};

export type RequestBody = Record<string, never>;

export type Response = Array<{
	Name: string;
	Id: string;
	Created: string;
	Scope: 'local' | 'global';
	Driver: string;
	EnableIPv6: boolean;
	IPAM: {
		Driver: string;
		Config: Array<Record<string, string>>;
		Options: Record<string, string>;
	};
	Internal: boolean;
	Attachable: boolean;
	Ingress: boolean;
	Containers: Record<
		string,
		{
			Name: string;
			EndpointID: string;
			MacAddress: string;
			IPv4Address: string;
			IPv6Address: string;
		}
	>;
	Options: Record<string, string>;
	Labels: Record<string, string>;
}>;
