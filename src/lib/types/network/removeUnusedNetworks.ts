export type PathParams = Record<string, never>;

export type QueryParams = {
	filters?: string;
};

export type RequestBody = Record<string, never>;

export type Response = {
	NetworksDeleted: string[];
};
