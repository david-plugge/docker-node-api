export type PathParams = {
	id: string;
};

export type QueryParams = Record<string, never>;

export type RequestBody = {
	Container: string;
	EndpointConfig?: {
		IPAMConfig?: {
			Links?: string[];
			Aliases?: string[];
			NetworkID?: string;
			EndpointID?: string;
			Gateway?: string;
			IPAddress?: string;
			IPPrefixLen?: number;
			IPv6Gateway?: string;
			GlobalIPv6Address?: string;
			GlobalIPv6PrefixLen?: number;
			MacAddress?: string;
			DriverOpts?: Record<string, string>;
		};
	};
};

export type Response = Record<string, never>;
