export type PathParams = {
	id: string;
};

export type QueryParams = {
	verbose?: boolean;
	scope?: 'swarm' | 'global' | 'local';
};

export type RequestBody = Record<string, never>;

export type Response = {
	Name: string;
	Id: string;
	Created: string;
	Scope: 'local' | 'global';
	Driver: string;
	EnableIPv6: boolean;
	IPAM: {
		Driver: string;
		Config: Array<Record<string, string>>;
		Options: Record<string, string>;
	};
	Internal: boolean;
	Attachable: boolean;
	Ingress: boolean;
	Containers: Record<
		string,
		{
			Name: string;
			EndpointID: string;
			MacAddress: string;
			IPv4Address: string;
			IPv6Address: string;
		}
	>;
	Options: Record<string, string>;
	Labels: Record<string, string>;
};
