export type PathParams = {
	id: string;
};

export type QueryParams = Record<string, never>;

export type RequestBody = {
	Container: string;
	Force: boolean;
};

export type Response = Record<string, never>;
