export type PathParams = {
	id: string;
};

export type QueryParams = Record<string, never>;

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
