export type PathParams = Record<string, never>;

export type QueryParams = Record<string, never>;

export type RequestBody = {
	Name: string;
	CheckDuplicate?: boolean;
	Driver?: string;
	Internal?: boolean;
	Attachable?: boolean;
	Ingress?: boolean;
	IPAM?: {
		Driver?: string;
		Config?: Array<Record<string, string>>;
		Options?: Record<string, string>;
	};
	EnableIPv6?: boolean;
	Options?: Record<string, string>;
	Labels?: Record<string, string>;
};

export type Response = {
	Id: string;
	Warning: string;
};
