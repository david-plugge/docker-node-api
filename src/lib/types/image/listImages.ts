export type PathParams = Record<string, never>;

export type QueryParams = {
	all?: boolean;
	digests?: boolean;
	filters?: {
		before?: string[];
		dangling?: boolean;
		label?: string[];
		reference?: string[];
		since?: string[];
	};
};

export type RequestBody = Record<string, never>;

export type Response = Array<{
	Id: string;
	ParentId: string;
	RepoTags: string;
	RepoDigests: string;
	Created: number;
	Size: number;
	SharedSize: number;
	VirtualSize: number;
	Labels: Record<string, string>;
	Containers: number;
}>;
