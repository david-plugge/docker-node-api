export type PathParams = {
	name: string;
};

export type QueryParams = {
	force?: boolean;
	noprune?: string;
};

export type RequestBody = Record<string, never>;

export type Response = Array<{
	Untagged: string;
	Deleted: string;
}>;
