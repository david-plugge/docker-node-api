export type PathParams = never;

export type QueryParams = {
	fromImage?: string;
	tag?: string;
	platform?: string;
};

export type RequestHeaders = {
	'X-Registry-Auth': string;
};

export type RequestBody = never;

export type Response = Record<string, never>;
