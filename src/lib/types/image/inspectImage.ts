export type PathParams = {
	name: string;
};

export type QueryParams = Record<string, never>;

export type RequestBody = Record<string, never>;

export type Response = {
	Id: string;
	RepoTags?: string[];
	RepoDigests?: string[];
	Parent: string;
	Comment: string;
	Created: string;
	Container: string;
	ContainerConfig?: {
		Hostname?: string;
		Domainname?: string;
		User?: string;
		AttachStdin?: boolean;
		AttachStdout?: boolean;
		AttachStderr?: boolean;
		ExposedPorts?: Record<string, Record<string, unknown>>;
		Tty?: boolean;
		OpenStdin?: boolean;
		StdinOnce?: boolean;
		Env?: string[];
		Cmd?: string | string[];
		Healthcheck?: {
			Test?: [] | ['NONE'] | ['CMD', ...string[]] | ['CMD-SHELL', ...string[]];
			Interval?: number;
			Timeout?: number;
			Retries?: number;
			StartPeriod?: number;
		};
		ArgsEscaped?: boolean;
		Image?: string;
		Volumes?: Record<string, Record<string, unknown>>;
		WorkingDir?: string;
		Entrypoint?: string | string[];
		NetworkDisabled?: boolean;
		MacAddress?: string;
		OnBuild?: string[];
		Labels?: Record<string, string>;
		StopSignal?: string;
		StopTimeout?: number;
		Shell?: string[];
	};
	DockerVersion: string;
	Author: string;
	Config?: {
		Hostname?: string;
		Domainname?: string;
		User?: string;
		AttachStdin?: boolean;
		AttachStdout?: boolean;
		AttachStderr?: boolean;
		ExposedPorts?: Record<string, Record<string, unknown>>;
		Tty?: boolean;
		OpenStdin?: boolean;
		StdinOnce?: boolean;
		Env?: string[];
		Cmd?: string | string[];
		Healthcheck?: {
			Test?: [] | ['NONE'] | ['CMD', ...string[]] | ['CMD-SHELL', ...string[]];
			Interval?: number;
			Timeout?: number;
			Retries?: number;
			StartPeriod?: number;
		};
		ArgsEscaped?: boolean;
		Image?: string;
		Volumes?: Record<string, Record<string, unknown>>;
		WorkingDir?: string;
		Entrypoint?: string | string[];
		NetworkDisabled?: boolean;
		MacAddress?: string;
		OnBuild?: string[];
		Labels?: Record<string, string>;
		StopSignal?: string;
		StopTimeout?: number;
		Shell?: string[];
	};
	Architecture: string;
	Os: string;
	OsVersion?: string;
	Size: number;
	VirtualSize: number;
	GraphDriver: {
		Name: string;
		Data: Record<string, string>;
	};
	RootFS: {
		Type: string;
		Layers?: string[];
		BaseLayer?: string;
	};
	MetaData: {
		LastTagTime: string;
	};
};
