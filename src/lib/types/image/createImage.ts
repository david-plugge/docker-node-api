export type PathParams = never;

export type QueryParams = {
	fromSrc?: string;
	repo?: string;
	tag?: string;
	message?: string;
	platform?: string;
};

export type RequestBody = '' | string;

export type RequestHeaders = {
	'X-Registry-Auth'?: string;
};
export type Response = {
	Id: string;
	ParentId: string;
	RepoTags: string;
	RepoDigests: string;
	Created: number;
	Size: number;
	SharedSize: number;
	VirtualSize: number;
	Labels: Record<string, string>;
	Containers: number;
};
