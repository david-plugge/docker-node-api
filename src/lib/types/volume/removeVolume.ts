export type PathParams = {
	name: string;
};

export type QueryParams = {
	force?: boolean;
};

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
