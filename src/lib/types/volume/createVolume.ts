export type PathParams = Record<string, never>;

export type QueryParams = Record<string, never>;

export type RequestBody = {
	Name?: string;
	Driver?: string;
	DriverOpts?: Record<string, string>;
	Labels?: Record<string, string>;
};

export type Response = {
	Name: string;
	Driver: string;
	Mountpoint: string;
	CreatedAt?: string;
	Status?: Record<string, string>;
	Labels: Record<string, string>;
	Scope: 'local' | 'global';
	Options: Record<string, string>;
	UsageData?: {
		Size: number;
		RefCount: number;
	};
};
