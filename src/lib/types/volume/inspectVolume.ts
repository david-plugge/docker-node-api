export type PathParams = {
	name: string;
};

export type QueryParams = Record<string, never>;

export type RequestBody = Record<string, never>;

export type Response = {
	Name: string;
	Driver: string;
	Mountpoint: string;
	CreatedAt?: string;
	Status?: Record<string, string>;
	Labels: Record<string, string>;
	Scope: 'local' | 'global';
	Options: Record<string, string>;
	UsageData?: {
		Size: number;
		RefCount: number;
	};
};
