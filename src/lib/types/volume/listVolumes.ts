export type PathParams = Record<string, never>;

export type QueryParams = {
	filters?: string;
};

export type RequestBody = Record<string, never>;

export type Response = {
	Volumes: Array<{
		Name: string;
		Driver: string;
		Mountpoint: string;
		CreatedAt?: string;
		Status?: Record<string, string>;
		Labels: Record<string, string>;
		Scope: 'local' | 'global';
		Options?: Record<string, string>;
		UsageData: {
			Size: number;
			RefCount: number;
		};
	}>;
	Warnings: string[];
};
