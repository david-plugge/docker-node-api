export type EventType =
	| 'container'
	| 'image'
	| 'volume'
	| 'network'
	| 'daemon'
	| 'plugin'
	| 'node'
	| 'service'
	| 'secret'
	| 'config';
export type EventAction =
	| 'attach'
	| 'commit'
	| 'copy'
	| 'create'
	| 'destroy'
	| 'detach'
	| 'die'
	| 'exec_create'
	| 'exec_detach'
	| 'exec_start'
	| 'exec_die'
	| 'export'
	| 'health_status'
	| 'kill'
	| 'oom'
	| 'pause'
	| 'rename'
	| 'resize'
	| 'restart'
	| 'start'
	| 'stop'
	| 'top'
	| 'unpause'
	| 'update'
	| 'prune';

export type PathParams = Record<string, never>;

export type QueryParams = {
	since?: string;
	until?: string;
	filters?: {
		config?: string[];
		container?: string[];
		daemon?: string[];
		event?: EventAction[];
		image?: string[];
		label?: string[];
		network?: string[];
		node?: string[];
		plugin?: string[];
		scope?: string[];
		secret?: string[];
		service?: string[];
		type?: EventType[];
		volume?: string[];
	};
};

export type RequestBody = Record<string, never>;

export type Response = {
	Type: EventType;
	Action: EventAction;
	Actor: {
		ID: string;
		Attributes: Record<string, string>;
	};
	time: number;
	timeNano: number;
};
