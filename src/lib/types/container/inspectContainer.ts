export type PathParams = {
	id: string;
};

export type QueryParams = {
	size?: boolean;
};

export type RequestBody = Record<string, never>;

export type Response = {
	Id?: string;
	Created?: string;
	Path?: string;
	Args?: string[];
	State?: {
		Status?: 'created' | 'running' | 'paused' | 'restarting' | 'removing' | 'exited' | 'dead';
		Running?: boolean;
		Paused?: boolean;
		Restarting?: boolean;
		OOMKilled?: boolean;
		Dead?: boolean;
		Pid?: number;
		ExitCode?: number;
		Error?: string;
		StartedAt?: string;
		FinishedAt?: string;
		Health?: {
			Status?: 'none' | 'starting' | 'healthy' | 'unhealthy';
			FailingStreak?: number;
			Log?: Array<{
				Start?: string;
				End?: string;
				ExitCode?: number;
				Output?: string;
			}>;
		};
	};
	Image?: string;
	ResolvConfPath?: string;
	HostnamePath?: string;
	HostsPath?: string;
	LogPath?: string;
	Name?: string;
	RestartCount?: number;
	Driver?: string;
	Platform?: string;
	MountLabel?: string;
	ProcessLabel?: string;
	AppArmorProfile?: string;
	ExecIDs?: string[];
	HostConfig?: {
		CpuShares?: number;
		Memory?: number;
		CgroupParent?: string;
		BlkioWeight?: number;
		BlkioWeightDevice?: Array<{ Path?: string; weight?: number | string }>;
		BlkioDeviceReadBps?: Array<{ Path?: string; Rate?: number | string }>;
		BlkioDeviceWriteBps?: Array<{ Path?: string; Rate?: number | string }>;
		BlkioDeviceReadIOps?: Array<{ Path?: string; Rate?: number | string }>;
		BlkioDeviceWriteIOps?: Array<{ Path?: string; Rate?: number | string }>;
		CpuPeriod?: number;
		CpuQuota?: number;
		CpuRealtimePeriod?: number;
		CpuRealtimeRuntime?: number;
		CpusetCpus?: string;
		CpusetMems?: string;
		Devices?: Array<{ PathOnHost?: string; PathInContainer?: string; CgroupPermissions?: string }>;
		DeviceCgroupRules?: string[];
		DeviceRequests?: Array<{
			Driver?: string;
			Count?: number;
			DeviceIDs?: string[];
			Capabilities?: string[];
			Options?: Record<string, string>;
		}>;
		KernelMemory?: number;
		KernelMemoryTCP?: number;
		MemoryReservation?: number;
		MemorySwap?: number;
		MemorySwappiness?: number;
		NanoCpus?: number;
		OomKillDisable?: boolean;
		Init?: boolean;
		PidsLimit?: number;
		Ulimits?: Array<{ Name?: string; Soft?: number; Hard?: number }>;
		CpuCount?: number;
		CpuPercent?: number;
		IOMaximumIOps?: number;
		IOMaximumBandwidth?: number;
		Binds?: string[];
		ContainerIDFile?: string;
		LogConfig?: {
			Type?:
				| 'json-file'
				| 'syslog'
				| 'journald'
				| 'gelf'
				| 'fluentd'
				| 'awslogs'
				| 'splunk'
				| 'etwlogs'
				| 'none';
			Config?: Record<string, string>;
		};
		NetworkMode?: 'bridge' | 'host' | 'none' | 'string';
		PortBindings?: Record<string, Array<{ HostIp?: string; HostPort?: string }>>;
		RestartPolicy?:
			| { Name?: '' | 'always' | 'unless-stopped' }
			| { Name?: 'on-failure'; MaximumRetryCount?: number };
		AutoRemove?: boolean;
		VolumeDriver?: string;
		VolumesFrom?: string[];
		Mounts?: Array<{
			Target?: string;
			Source?: string;
			Type?: 'bind' | 'volume' | 'tmpfs' | 'npipe';
			Readonly?: boolean;
			Consistency?: string;
			BindOptions?: {
				Propagation?: 'private' | 'rprivate' | 'shared' | 'rshared' | 'slave' | 'rslave';
				NonRecursive?: boolean;
			};
			VolumeOptions?: {
				NoCopy?: boolean;
				Labels?: Record<string, string>;
				DriverConfig?: {
					Name?: string;
					Options?: Record<string, string>;
				};
			};
			TmpfsOptions?: {
				SizeBytes?: number;
				Mode?: number;
			};
		}>;
		CapAdd?: string[];
		CapDrop?: string[];
		CgroupnsMode?: 'private' | 'host';
		Dns?: string[];
		DnsOptions?: string[];
		DnsSearch?: string[];
		ExtraHosts?: string[];
		GroupAdd?: string[];
		IpcMode?: 'none' | 'private' | 'shareable' | 'host' | string;
		Cgroup?: string;
		Links?: string[];
		OomScoreAdj?: number;
		PidMode?: 'host' | string;
		Privileged?: boolean;
		PublishAllPorts?: boolean;
		ReadonlyRootfs?: boolean;
		SecurityOpt?: string[];
		StorageOpt?: Record<string, string>;
		Tmpfs?: Record<string, string>;
		UTSMode?: string;
		UsernsMode?: string;
		ShmSize?: number;
		Sysctls?: Record<string, string>;
		Runtime?: string;
		ConsoleSize?: number[];
		Isolation?: 'default' | 'process' | 'hyperv';
		MaskedPaths?: string[];
		ReadonlyPaths?: string[];
	};
	GraphDriver?: {
		Name?: string;
		Data?: Record<string, string>;
	};
	SizeRw?: number;
	SizeRootFs?: number;
	Mounts?: [
		{
			Type?: string;
			Name?: string;
			Source?: string;
			Destination?: string;
			Driver?: string;
			Mode?: string;
			RW?: boolean;
			Propagation?: string;
		}
	];
	Config?: {
		Hostname?: string;
		Domainname?: string;
		User?: string;
		AttachStdin?: boolean;
		AttachStdout?: boolean;
		AttachStderr?: boolean;
		ExposedPorts?: Record<string, Record<string, unknown>>;
		Tty?: boolean;
		OpenStdin?: boolean;
		StdinOnce?: boolean;
		Env?: string[];
		Cmd?: string | string[];
		Healthcheck?: {
			Test?: [] | ['NONE'] | ['CMD', ...string[]] | ['CMD-SHELL', ...string[]];
			Interval?: number;
			Timeout?: number;
			Retries?: number;
			StartPeriod?: number;
		};
		ArgsEscaped?: boolean;
		Image?: string;
		Volumes?: Record<string, Record<string, unknown>>;
		WorkingDir?: string;
		Entrypoint?: string | string[];
		NetworkDisabled?: boolean;
		MacAddress?: string;
		OnBuild?: string[];
		Labels?: Record<string, string>;
		StopSignal?: string;
		StopTimeout?: number;
		Shell?: string[];
	};
	NetworkSettings?: {
		Bridge?: string;
		SandboxID?: string;
		HairpinMode?: boolean;
		LinkLocalIPv6Address?: string;
		LinkLocalIPv6PrefixLen?: number;
		Ports?: Record<string, Array<{ HostIp?: string; HostPort?: string }>>;
		SandboxKey?: string;
		SecondaryIPAddresses?: string[];
		SecondaryIPv6Addresses?: string[];
		EndpointID?: string;
		Gateway?: string;
		GlobalIPv6Address?: string;
		GlobalIPv6PrefixLen?: number;
		IPAddress?: string;
		IPPrefixLen?: number;
		IPv6Gateway?: string;
		MacAddress?: string;
		Networks?: Record<
			string,
			{
				IPAMConfig?: {
					Links?: string[];
					Aliases?: string[];
					NetworkID?: string;
					EndpointID?: string;
					Gateway?: string;
					IPAddress?: string;
					IPPrefixLen?: number;
					IPv6Gateway?: string;
					GlobalIPv6Address?: string;
					GlobalIPv6PrefixLen?: number;
					MacAddress?: string;
					DriverOpts?: Record<string, string>;
				};
			}
		>;
	};
};
