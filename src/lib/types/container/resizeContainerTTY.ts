export type PathParams = {
	id: string;
};

export type QueryParams = { h?: number; w?: number };

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
