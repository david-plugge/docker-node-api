export type PathParams = {
	id: string;
};

export type QueryParams = {
	detachKeys?: string;
};

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
