export type PathParams = {
	id: string;
};

export type QueryParams = {
	t?: number;
};

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
