export type PathParams = {
	id: string;
};

export type QueryParams = Record<string, never>;

export type RequestBody = {
	CpuShares?: number;
	Memory?: number;
	CgroupParent?: string;
	BlkioWeight?: number;
	BlkioWeightDevice?: Array<{ Path?: string; weight?: number | string }>;
	BlkioDeviceReadBps?: Array<{ Path?: string; Rate?: number | string }>;
	BlkioDeviceWriteBps?: Array<{ Path?: string; Rate?: number | string }>;
	BlkioDeviceReadIOps?: Array<{ Path?: string; Rate?: number | string }>;
	BlkioDeviceWriteIOps?: Array<{ Path?: string; Rate?: number | string }>;
	CpuPeriod?: number;
	CpuQuota?: number;
	CpuRealtimePeriod?: number;
	CpuRealtimeRuntime?: number;
	CpusetCpus?: string;
	CpusetMems?: string;
	Devices?: Array<{ PathOnHost?: string; PathInContainer?: string; CgroupPermissions?: string }>;
	DeviceCgroupRules?: string[];
	DeviceRequests?: Array<{
		Driver?: string;
		Count?: number;
		DeviceIDs?: string[];
		Capabilities?: string[];
		Options?: Record<string, string>;
	}>;
	KernelMemory?: number;
	KernelMemoryTCP?: number;
	MemoryReservation?: number;
	MemorySwap?: number;
	MemorySwappiness?: number;
	NanoCpus?: number;
	OomKillDisable?: boolean;
	Init?: boolean;
	PidsLimit?: number;
	Ulimits?: Array<{ Name?: string; Soft?: number; Hard?: number }>;
	CpuCount?: number;
	CpuPercent?: number;
	IOMaximumIOps?: number;
	IOMaximumBandwidth?: number;
	RestartPolicy?:
		| { Name?: '' | 'always' | 'unless-stopped' }
		| { Name?: 'on-failure'; MaximumRetryCount?: number };
};

export type Response = {
	Warnings: string[];
};
