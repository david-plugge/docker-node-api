export type PathParams = {
	id: string;
};

export type QueryParams = {
	ps_args?: string;
};

export type RequestBody = Record<string, never>;

export type Response = {
	Titels?: string[];
	Processes?: string[][];
};
