export type PathParams = {
	id: string;
};

export type QueryParams = {
	name: string;
};

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
