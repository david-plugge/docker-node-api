export type PathParams = {
	id: string;
};

export type QueryParams = {
	v?: boolean;
	force?: boolean;
	link?: boolean;
};

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
