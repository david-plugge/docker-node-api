export type PathParams = {
	id: string;
};

export type QueryParams = {
	follow?: boolean;
	stdout?: boolean;
	stderr?: boolean;
	since?: number;
	until?: boolean;
	timestamps?: boolean;
	tail?: 'all' | number;
};

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
