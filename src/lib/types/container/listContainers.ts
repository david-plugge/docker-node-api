export type PathParams = Record<string, never>;

export type QueryParams = {
	all?: boolean;
	limit?: number;
	size?: boolean;
	filters?: {
		ancestor?: string[];
		before?: string[];
		expose?: string[];
		exited?: number[];
		health?: Array<'starting' | 'healthy' | 'unhealthy' | 'none'>;
		id?: string[];
		isolation?: string[];
		'is-task'?: boolean;
		label?: string[];
		name?: string[];
		network?: string[];
		publish?: string[];
		since?: string[];
		status?: Array<
			'created' | 'restarting' | 'running' | 'removing' | 'paused' | 'exited' | 'dead'
		>;
		volume?: string[];
	};
};

export type RequestBody = Record<string, never>;

export type Response = Array<{
	Id: string;
	Names: string[];
	Image: string;
	ImageId: string;
	Command: string;
	Created: number;
	Ports: Array<{
		IP: string;
		PrivatePort: number;
		PublicPort: number;
		Type: 'tcp' | 'udp' | 'sctp';
	}>;
	SizeRW: number;
	SizeRootFs: number;
	Labels: Record<string, string>;
	State: string;
	Status: string;
	HostConfig: {
		NetworkMode: string;
	};
	NetworkSettings: {
		Networks: Record<
			string,
			{
				Links: string[];
				Aliases: string[];
				NetworkID: string;
				EndpointID: string;
				Gateway: string;
				IPAddress: string;
				IPPrefixLen: number;
				IPv6Gateway: string;
				GlobalIPv6Address: string;
				GlobalIPv6PrefixLen: number;
				MacAddress: string;
				DriverOpts: Record<string, string>;
			}
		>;
	};
	Mounts: {
		Target: string;
		Source: string;
		Type: 'bind' | 'volume' | 'tmpfs' | 'npipe';
		Readonly: boolean;
		Consistency: string;
		BindOptions: {
			Propagation: string;
			NonRecursive: boolean;
		};
		VolumeOptions: {
			NoCopy: boolean;
			Labels: Record<string, string>;
			DriverConfig: {
				Name: string;
				Options: Record<string, string>;
			};
		};
		TmpfsOptions: {
			SizeBytes: number;
			Mode: number;
		};
	};
}>;
