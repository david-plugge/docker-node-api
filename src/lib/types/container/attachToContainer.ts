export type PathParams = {
	id: string;
};

export type QueryParams = {
	detachKeys?: string;
	logs?: boolean;
	stream?: boolean;
	stdin?: boolean;
	stdout?: boolean;
	stderr?: boolean;
};

export type RequestBody = Record<string, never>;

export type Response = Record<string, never>;
